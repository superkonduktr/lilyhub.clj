# Some notes

### Starting
In repl:
```
(run)
```

### Migrations
Using [clj-sql-up]:
```sh
$ ENV=dev lein clj-sql-up migrate
```
```sh
$ ENV=test lein clj-sql-up rollback
```
etc.

### Stylesheets
Precompile CSS with [garden].
```sh
$ lein garden once
```
Or compile continuously:
```sh
$ lein garden auto
```

### TODO
* Tests.
* Fix timestamps.
* Fix everything else.

[clj-sql-up]:https://github.com/ckuttruff/clj-sql-up
[garden]:https://github.com/noprompt/lein-garden
