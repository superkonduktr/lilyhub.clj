(ns user
  (:require [leiningen.core.main :as lein]
            [clojure.tools.logging :as log]
            [lh.system :refer [system]]
            [com.stuartsierra.component :as component])
  (:gen-class))

(defn start-figwheel []
  (future
    (print "Starting figwheel.\n")
    (lein/-main ["figwheel"])))

(defn run []
  "Starts server on port 8000 with db connection."
  (-> (system) (component/start))
  (start-figwheel))
