(ns lh.server-test
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.test :refer :all]
            [com.stuartsierra.component :as component]
            [lh.db-query :as db]
            [lh.server :as server]
            [lh.system :as system]
            [ring.mock.request :as mock]))

(def ^:dynamic *system* nil)

(defn with-system
  [f]
  (binding [*system* (-> (system/system :test)
                         (component/start))]
    (try
      (f)
      (finally
        (component/stop *system*)))))

(defn truncate-db
  [f]
  (let [db (:db *system*)
        all-tables (-> {:select [:*]
                        :from [:information_schema.tables]
                        :where [:and
                                [:= :table_schema "public"]
                                [:= :table_type "BASE TABLE"]]}
                       (db/query db))
        names (-> (into #{} (map :table_name all-tables))
                  ;; не чистить таблички с миграциями. первая рельсовая, вторая от
                  ;; flywayqdb
                  (disj "schema_migrations" "schema_version" "clj_sql_migrations"))]
    (doseq [n names]
      (jdbc/execute! db [(str "TRUNCATE TABLE \"" n "\" CASCADE")])))
  (f))

(use-fixtures :each with-system truncate-db)

(deftest create-good-engraving
  (let [server (server/app-routes (:db *system*))
        created (server/with-synchrony
                  (:body (server (-> (mock/request :post "/publish")
                                 (assoc :params {:text "{ c d e2 }"
                                                 :private false})))))
        id (:id created)
        req (server (mock/request :get "/browse/1"))]

    (is (string? id))
    (is (= (re-matches #"[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"
                       id)
           id))

    (is (= 200 (req :status)))
    (is (= {} (req :headers)))
    (is (sequential? (get-in req [:body :content])))

    (let [content (-> (get-in req [:body :content]) first)]
      (is (= id (:id content)))
      (is (not (nil? (re-matches #"(a moment|1 second|\d{1,2} seconds) ago"
                                 (:updated_at content)))))
      (is (not (nil? (re-matches #"(a moment|1 second|\d{1,2} seconds) ago"
                                 (:created_at content)))))
      (is (false? (:is_private content)))
      (is (= "{ c d e2 }" (:text content)))
      (is (= "completed" (:state content)))
      (is (nil? (:error content))))))

(deftest create-bad-engraving
  (let [server (server/app-routes (:db *system*))
        created (server/with-synchrony
                  (:body (server (-> (mock/request :post "/publish")
                                     (assoc :params {:text "{ r3"
                                                     :private false})))))
        id (:id created)
        req (server (mock/request :get (str "/engravings/" id)))]

    (is (map? req))

    (is (= 200 (req :status)))
    (is (= {} (req :headers)))
    (let [body (:body req)]
      (is (= id (:id body)))
      (is (false? (:is_private body)))
      (is (not (nil? (re-matches #"(a moment|1 second|\d{1,2} seconds) ago"
                                 (:created_at body)))))
      (is (not (nil? (re-matches #"(a moment|1 second|\d{1,2} seconds) ago"
                                 (:updated_at body)))))
      (is (= "{ r3" (:text body)))
      (is (= "error" (:state body)))
      (is (= [{:line 1, :msg "not a duration"}
              {:line 1, :msg "syntax error, unexpected end of input"}]
             (:error body))))))

(deftest not-found
  (let [server (server/app-routes (:db *system*))]

    (is (= {:status 404 :headers {} :body nil}
           (server (mock/request :get "/browse/1"))))

    (is (= {:status 404 :headers {} :body nil}
           (server (mock/request :get "/engravings/non-existent"))))))