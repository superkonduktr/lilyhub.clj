(ns lh.helper-test
  (require [clojure.test :refer :all]
           [lh.helper :refer :all]))

(deftest humans-vs-machines
  (let [ts1 (->> (java.util.Date.) (.getTime) (java.sql.Timestamp.))
        ts2 (do (Thread/sleep 1000)
                (->> (java.util.Date.) (.getTime) (java.sql.Timestamp.)))
        engr {:text "foo"
              :created_at ts1
              :updated_at ts2}]
    (is (= {:text "foo"
            :created_at "1 second ago"
            :updated_at "a moment ago"}
           (humanize-timestamps engr)))))
