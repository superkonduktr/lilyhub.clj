(ns lh.lilypond-test
  (require [clojure.test :refer :all]
           [lh.lilypond :refer [render]])
  (:import [java.io File]))

(defn cleanup [f]
  (try
    (f)
    (finally
      (map #(.delete (File. (str "resources/public/img/engravings/test." %)))
           ["preview.eps" "png" "preview.png"]))))

(use-fixtures :each cleanup)

(deftest render-good
  (is (= (render "{ c d e2 }" "test")
         {:status "completed" :error []})))

(deftest render-with-errors
  (is (= (render "bad : syntax {\npoorMusicEducation r6" "test")
         {:status "error",
          :error [{:line 1, :msg "syntax error, unexpected ':'"}
                  {:line 2, :msg "unrecognized string, not in text script or \\lyricmode"}
                  {:line 2, :msg "not a duration"}
                  {:line 2, :msg "syntax error, unexpected end of input"}]})))