;; migrations/20150122022421411-create-errors.clj

(defn up []
  ["CREATE TABLE IF NOT EXISTS errors (id SERIAL PRIMARY KEY,
                                       engraving_id varchar(36) NOT NULL,
                                       line int,
                                       msg text);"])

(defn down []
  ["DROP TABLE IF EXISTS errors;"])
