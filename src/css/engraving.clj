(ns lh.css.engraving
  (:require [garden.color :as color :refer [darken lighten]]
            [lh.css.global :refer [global]]))

(defn engraving []
  (let [{main :main alt :alt code-bg :code-bg} (global :colors)
        font-code (-> global :fonts :code)]
    [[:.engraving-container {:margin "50px 0"}
       [:header {:background (color/lighten alt 25)
                       :color (color/darken alt 50)
                       :padding "10px 20px"
                       :margin "0 0 30px"}
         [:a {:color (color/darken alt 50)}
           [:&:hover {:color "#555"}]]]
       [:.engraving-body {:margin "30px 0"}]
       [:.engraving-control-panel {:margin "20px 0"}
         [:a {:text-decoration "dotted"
              :border-bottom "1px dotted"}]]
       [:.glyphicon-remove {:color "#666"}
         [:&:hover {:cursor "pointer"
                    :color "#999"}]]]
     [:.engraving-code {:display "none"
                        :padding "10px 20px 10px 0"
                        :background code-bg}
       [:pre {:background: code-bg
              :color "#ddd"}]]
     [:.engraving-share {:display "none"
                         :padding "10px 20px"
                         :background (color/lighten main 30)}]
     [:.loader-container {:text-align "center"}
       [:p :img {:margin "15px auto"}]]
     [:.engraving-stub {:border "1px solid #dfdfdf"
                        :background "url(/img/engraving-stub.png)"
                        :width "100%"
                        :height "414px"}
       [:#overlay {:width "inherit"
                   :height "inherit"
                   :border-radius "inherit"
                   :background "#fff"
                   :opacity 0.9
                   :vertical-align "middle"
                   :position "relative"}
         [:#overlay-text {:color "#333"
                          :text-shadow "1px 1px 10px rgba(200, 200, 200, 0.2)"
                          :width "60%"
                          :height "50%"
                          :margin "auto"
                          :position "absolute"
                          :top 0
                          :left 0
                          :bottom 0
                          :right 0}]]]
     [:ul.error-list {:list-style-type "none"
                      :padding "15px 25px"}
       [:li {:font-family font-code
             :font-size "0.8em"}]]]))
