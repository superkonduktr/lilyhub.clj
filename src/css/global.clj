(ns lh.css.global)

(def global
  {:colors {:main "#77b3c7"
            :alt "#e18870"
            :alt-bg "#333"
            :code-bg "#2d2d2d"}
   :fonts {:import ["http://fonts.googleapis.com/css?family=Oxygen"
                    "http://fonts.googleapis.com/css?family=Lobster"
                    "http://fonts.googleapis.com/css?family=Pacifico"]
           :sans-serif "Oxygen, \"Open Sans\", Helvetica, sans-serif"
           :body "Oxygen, Helvetica, sans-serif"
           :code "Monaco, Menlo, \"Ubuntu Mono\", Consolas, \"source-code-pro\", \"Courier New\", monospace"}})
