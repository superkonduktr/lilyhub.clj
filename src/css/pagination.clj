(ns lh.css.pagination)

(defn pagination []
  [[:.pagination {:width "100%"
                  :padding "0 32px"
                  :font-size "0.9em"}]
   [:.next-page {:float "right"}]])
