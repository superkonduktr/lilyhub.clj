(ns lh.css.general
  (:require [garden.color :as color :refer [darken lighten]]
            [lh.css.global :refer [global]]))

(defn general []
  (let [{main :main alt :alt} (global :colors)]
    [[:html :body {:margin 0
                 :padding 0
                 :height "100%"}]
     [:#app {:height "100%"}]
     [:body {:font-family (:body (global :fonts))
             :font-size "16px"
             :line-height "1.6em"}]
     [:.row {:padding "10px 0"}]
     [:.wrapper {:min-height "100%"
                 :position "relative"}]
     [:.content {:padding "10px 10px 200px"}]
     [:.app-loader {:width "64px"
                    :margin "250px auto"}]
     [:h1 {:margin "60px 0 40px"}]
     [:h2 :h3 {:margin "60px 0 35px"
               :font-weight "bold"}]
     [:h4 :h5 :h6 {:margin-bottom "30px"
                   :font-weight "bold"}]
     [:h1 :h2 :h3 :h4 :h5 :h6 {:color (color/darken main 45)}]
     [:a {:color (color/darken main 10)}
       [:&:hover :&:focus {:color (color/lighten main 10)
                           :text-decoration "none"}]]
     [:p {:margin "10px 0 20px"}]
     [:pre {:border-radius 0
            :border 0
            :padding "15px 25px"
            :margin "20px 0"
            :background (color/lighten alt 32)}]
     [:code {:color (color/darken alt 10)
             :background (color/lighten alt 32)}]
     [:.btn {:padding "10%"
             :text-align "center"
             :border "1px solid #aaa"
             :border-radius 0
             :background "#fff"
             :width "100%"
             :font-size "1em"}
       [:&:hover {:background "#eee"
                 :cursor "pointer"}]]
     [:.btn-publish {:color (color/lighten alt 80)
                     :background alt
                     :border "1px solid"
                     :border-color (color/darken alt 3)}
       [:&:hover {:color (color/lighten alt 80)
                  :background (color/darken alt 8)
                  :border-color (color/darken alt 10)}]]
     [:.btn-checkbox.checked {:background (color/lighten main 10)
                              :color (color/lighten main 80)
                              :border-color (color/lighten main 7)}]
     [:.form-hidden {:display "none"}]]))

