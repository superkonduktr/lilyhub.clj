(ns lh.css.header
  (:require [garden.color :as color :refer [darken lighten]]
            [lh.css.global :refer [global]]))

(defn header []
  (let [main (-> global :colors :main)]
    [[:.header-navbar {:padding "10px"
                       :border-radius 0
                       :background "#333"
                       :color "#ddd"}]
     [:.navbar-brand {:margin "25px 0"}
       [:a {:font-family "Pacifico, Lobster, Georgia" ; FIXME
            :font-size "3em"
            :color (:main (global :colors))}
         [:&:hover {:text-decoration "none"
                    :color (color/darken main 10)}]]]
     [:.header-links
       [:ul {:list-style-type "none"
             :margin "8% 0"
             :font-size "1.1em"
             :padding 0}]
       [:li {:display "inline-block"
             :margin "0 1em"}]
       [:a {:color "#aaa"}
         [:&:hover {:color "#eee"
                    :text-decoration "none"}]
         [:&.active {:color (color/lighten main 10)}
           [:&:hover {:color (color/lighten main 20)}]]]]]))
