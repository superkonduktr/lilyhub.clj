(ns lh.css.homepage
  (:require [garden.color :as color :refer [darken lighten]]
            [lh.css.global :refer [global]]))

(defn homepage []
  (let [code-bg (-> global :colors :code-bg)
        font-code (-> global :fonts :code)]
    [[:.panel-new-engraving {:margin "15px 0"}]
     [:.input-wrapper [:margin "30px 0"
                       :background code-bg]]
     [:form.new-engraving
       [:#ace-new-engraving :textarea {:margin "7px 0"
                                       :height "400px"}]
       [:textarea {:color "#ddd"
                   :font-family font-code
                   :font-size "0.8em"
                   :background code-bg
                   :border-radius 0
                   :resize "none"
                   :height "414px"}]]]))
