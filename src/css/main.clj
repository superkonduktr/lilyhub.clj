(ns lh.css.main
  (:require [garden.def :refer [defstylesheet defstyles]]
            [garden.stylesheet :refer [at-import]]
            [lh.css.global :refer :all]
            [lh.css.general :refer [general]]
            [lh.css.header :refer [header]]
            [lh.css.footer :refer [footer]]
            [lh.css.homepage :refer [homepage]]
            [lh.css.engraving :refer [engraving]]
            [lh.css.pagination :refer [pagination]]
            [lh.css.guide :refer [guide]]
            [lh.css.responsive :refer [responsive]]))

(defn import-fonts [fonts]
  (map at-import fonts))

;; Gather all the rules here.

(defstyles style
  {:output-to "resources/public/css/style.css"}
  (import-fonts (:import (global :fonts)))
  (general)
  (header)
  (footer)
  (homepage)
  (engraving)
  (pagination)
  (responsive))
