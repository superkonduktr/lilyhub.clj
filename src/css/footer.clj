(ns lh.css.footer
  (:require [garden.color :as color :refer [darken lighten]]
            [lh.css.global :refer [global]]))

(defn footer []
  (let [main (-> global :colors :main)]
    [[:footer.page-footer {:background main
                           :width "100%"
                           :height "124px"
                           :position "absolute"
                           :bottom 0
                           :left 0
                           :padding "30px"
                           :margin-top "100px"
                           :color (color/darken main 60)}
       [:ul {:list-style-type "none"
             :margin "0"
             :padding "0"}
         [:li {:line-height "64px"
               :display "inline"
               :margin "0 20px 0 0"}]]
       [:div {:text-align "center"}]
       [:a {:color (color/darken main 60)}
         [:&:hover {:color (color/darken main 30)
                    :text-decoration "none"}]]]
     [:span.footer-qrest
       [:img {:opacity "0.2"}]]
     [:.footer-img-link {:opacity "0.8"}
       [:&:hover {:opacity "0.6"}]]]))
