(ns lh.css.guide
  (:require [garden.color :as color :refer [darken lighten]]
            [lh.css.global :refer [global]]))

(defn guide []
  (let [{main :main alt :alt code-bg :code-bg} (global :colors)]
    [[:#section-guide
       [:.row {:margin 0}]]
     [:.code-wrapper {:background code-bg
                      :padding "7px 0"
                      :margin "15px 0"}]
     [:.example-img-wrapper {:padding "20px 25px"}
       [:img :ul {:margin "0 auto"}]]
     [:ul.affix {:position "fixed"}]
     [:ul.affix-top {:position "static"}]
     [:ul.affix-bottom {:position "absolute"}]
     [:.sidenav {:margin-top "40px"
                 :margin-bottom "30px"
                 :padding-top "10px"
                 :padding-bottom "10px"
                 :border-radius "5px"}]
     [:.sidebar
       [:.nav
         [:li
           [:a {:display "block"
                :color "#333"
                :padding "5px 20px"}
             [:&:hover :&:focus {:text-decoration "none"
                                 :background-color (color/lighten alt 30)}]]
           [:&.active
             [:.nav {:display "block"}]]]
         [:.active ; .active:hover .active:focus
           [:a {:font-weight "bold"
                :color (color/darken main 20)
                :background-color "transparent"}]]
         [:.nav {:display "none"
                 :margin-bottom "8px"}
           [:li
             [:a {:padding-top "3px"
                  :padding-bottom "3px"
                  :padding-left "30px"
                  :font-size "0.9em"}]]]]]
     [:#content {:margin-top "20px"}]]))
