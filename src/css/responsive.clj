(ns lh.css.responsive
  (:require [garden.stylesheet :refer [at-media]]))

(defn responsive []
  (let [screen-sm-min "768px"
        screen-md-min "992px"
        screen-lg-min "1200px"]
    (at-media
      {:max-width screen-md-min}
      [:body {:font-size "2em"}]
      [:.navbar-brand {:width "100%"
                       :text-align "center"
                       :font-size "1.5em"}]
      [:.header-links
        [:ul {:text-align "center"}]]
      [:footer.page-footer {:height "252px"}]
      [:.content {:padding-bottom "330px"}])))
