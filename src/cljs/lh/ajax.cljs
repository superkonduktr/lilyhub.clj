(ns lh.cljs.ajax
  (:require [lh.cljs.session :as session]
            [lh.cljs.config :as config]
            [ajax.core :refer [GET POST]]
            [secretary.core :as secretary]))

;; Handler helpers.

(defn handler [response]
  (.log js/console (str response)))

(defn error-handler [{:keys [status status-text]}]
  (.log js/console (str "ERROR: " status " " status-text)))

(defn get-engraving-handler [response]
  (session/put! :show-engraving response)
  (session/put! :current-page :engraving))

(defn browse-handler [response]
  (session/put! :browse-engravings (:content response))
  (session/put! :browse-total (:total response))
  (session/put! :current-page :browse))

(defn preview-handler [response]
  (session/put! :new-engraving response))

(defn publish-handler [response]
  (let [{id :id} response]
    (secretary/dispatch! (str "/engravings/" id))
    (session/put! :new-engraving config/init-engraving)))

;; The actual requests.

(defn get-engraving
  "Requests a full record of engraving with given id."
  [id]
  (GET (str "/engravings/" id)
    {:handler get-engraving-handler
     :error-handler error-handler
     :response-format :transit}))

(defn browse-page
  "Requests a page of engravings with provided page number."
  ([] (browse-page 1))
  ([page]
   (GET (str "/browse/" page)
    {:handler browse-handler
     :error-handler error-handler
     :response-format :transit})))

(defn publish
  "Sends server a request to render given input.
  Accepts :preview as an option to render a preview."
  [& opts]
  (let [req {:error-handler error-handler
             :response-format :transit}
        handler (if (some #{:preview} opts) preview-handler publish-handler)
        text (session/get-new-engraving :text)
        private (session/get-new-engraving :private)
        params {:text text :private private}]
  (POST "/publish" (assoc req :handler handler :params params))))
