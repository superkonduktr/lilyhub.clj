(ns lh.cljs.views.browse
  (:require [lh.cljs.session :as session]
            [lh.cljs.views.engraving :refer [engraving-container]]))

;; Pagination helpers.

(defn total-pages []
  (let [engr-total (session/get :browse-total)]
    (if (= 0 (mod engr-total 10))
      (/ engr-total 10)
      (inc (quot engr-total 10)))))

(defn previous-page [page]
  (if-not (= 1 page)
    [:a.previous-page {:href (str "#/browse/" (dec page))} "Previous page"]))

(defn next-page [page]
  (if-not (> (inc page) (total-pages))
    [:a.next-page.pull-right {:href (str "#/browse/" (inc page))} "Next page"]))

(defn pagination [page]
  (let [page (js/parseInt page)]
    [:div.pagination.clearfix
      [previous-page page]
      [next-page page]]))

;; Browse content.

(defn browse [page engrs]
  [:div.container
    [:div.row
      [:div.col-lg-offset-1.col-lg-6.col-md-8.col-sm-12.col-xs-12
        [:h1 "Latest Engravings"]
        [pagination page]
        (map engraving-container engrs)
        [pagination page]]]])
