(ns lh.cljs.views.guide)

(defn guide []
  [:div.container
    [:div.row
      [:section.section-guide.col-lg-offset-1.col-lg-7.col-md-8.col-sm-12.col-xs-12
        [:h3 "Guide is complicated. I'll get to it later."]]]])
