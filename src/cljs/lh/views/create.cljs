(ns lh.cljs.views.create
  (:require [lh.cljs.ajax :as ajax]
            [lh.cljs.session :as session :refer [get-new-engraving put-new-engraving!]]
            [lh.cljs.views.engraving :refer [engraving-score]]
            [secretary.core :as secretary]))

(defn toggle-private []
  "Toggle is_private property of the engraving being created."
  (let [current (get-new-engraving :private)]
  (session/put-new-engraving! :private (not current))))

(defn set-private-checkbox-class []
  (let [private (get-new-engraving :private)]
    (if (true? private)
      "checked"
      "")))

(defn btn-checkbox []
  [:div.btn.btn-checkbox.btn-private "Private"])

(defn update-text [text]
  (session/put-new-engraving! :text text))

(defn preview []
  (session/put-new-engraving! :state "new")
  (ajax/publish :preview))

(defn publish []
  (session/put-new-engraving! :state "new")
  (ajax/publish))

(defn create []
  [:div.container
    [:div.row
      [:section.section-code.col-lg-offset-1.col-lg-5.col-md-6.col-sm-12.col-xs-12
        [:h3 "Write some LilyPond"]
        [:form.new-engraving
          [:textarea.form-control
           {:name "engraving-text"
            :rows 15
            :onChange #(update-text (-> % .-target .-value))}
           (get-new-engraving :text)]
          [:div.panel-new-engraving
            [:div.col-lg-4.col-md-4.col-sm-4.col-xs-4
              [:div.btn.btn-checkbox.btn-private
               {:onClick #(toggle-private)
                :class (set-private-checkbox-class)} "Private"]]
            [:div.col-lg-4.col-md-4.col-sm-4.col-xs-4
              [:div.btn {:onClick #(preview)}
                "Preview"]]
            [:div.col-lg-4.col-md-4.col-sm-4.col-xs-4
              [:div.btn.btn-publish {:onClick #(publish)}
                "Publish"]]]]]
      [:section.section-engraving.col-lg-5.col-md-6.col-sm-12.col-xs-12
        [:h3 "Engrave it"]
        [engraving-score (session/get :new-engraving)]]]])
