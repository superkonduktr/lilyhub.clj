(ns lh.cljs.views.engraving)

(defn engraving-img-url [id]
  (str "/img/engravings/" id ".png"))

(defn engraving-stub []
  [:div.engraving-stub
    [:div#overlay
      [:div#overlay-text
        [:p [:small "(Preview of your work will appear here.)"]]
        [:p "Not sure where to start?"
          [:br]
          "Try our " [:a {:href "#/guide"} "introductory guide"] "."]]]])

(defn loader-new-engraving []
  [:div.loader-container
    [:img.loader.loader-rendering {:src "/img/loader.gif" :alt "rendering"}]
    [:p "Hold on, it's rendering."]])

(defn error-list [error]
  [:div
    [:h4 "Something's wrong with your code."]
    [:ul.error-list
      (map
        (fn [e] [:li "line " (:line e) ": " (:msg e)])
        error)]])

(defn engraving-completed [id]
  [:img.img-responsive {:src (engraving-img-url id)
           :alt (str "engraving #" id)}])

(defn engraving-score [engr]
  (let [{id :id state :state error :error} engr]
    [:div.engraving-score {:data-engraving-id id :data-engraving-state state}
      (case state
        "new" [loader-new-engraving]
        "error" [error-list error]
        "completed" [engraving-completed id]
        [engraving-stub])]))


(defn engraving-container [engr]
  (let [{id :id text :text created :created_at} engr]
    [:div.engraving-container.clearfix
      [:header.clearfix
        [:small
          [:a.engraving-id {:href (str "#/engravings/" id)} id]]
        [:small.when.pull-right (str created)]]
      [:div.engraving-body
        [engraving-score engr]]
      [:div.engraving-code
        [:span.glyphicon.glyphicon-remove.pull-right]
        [:pre text]]
      [:div.engraving-share
        [:small "MAKSIMALNY RETWEET"]]
      [:div.engraving-control-panel
        [:ul.list-inline
          [:li
            [:a.toggle-code {:href "#"} "Show code"]]
          [:li
            [:a.toggle-share {:href "#"} "Share"]]]]]))

(defn engraving-page [engr]
  (let [{id :id private :is_private} engr]
    [:div.container
      [:div.row
        [:div.col-lg-offset-1.col-lg-6.col-md-8.col-sm-12.col-xs-12
          [:h2 (str "#" id)]
          (when private [:h4 [:em "(private)"]])
          [engraving-container engr]]]]))
