(ns lh.cljs.views.footer)

(defn footer []
  [:footer.page-footer
    [:div.container-fluid
      [:div.col-md-5.col-xs-12
        [:small
          [:ul.footer-links
            [:li "© 2014 LilyHub"]
            [:li [:a {:href "https://github.com/superkonduktr/LilyHub/blob/master/LICENSE"} "MIT License"]]
            [:li [:a {:href "http://lilypond.org"} "GNU LilyPond"]]]]]
      [:div.col-md-2
        [:span.footer-qrest [:img {:src "/img/quarter-rest-64.png" :alt "LilyHub"}]]]
      [:div.col-md-5.col-xs-12
        [:small
          [:ul.footer-links
            [:li [:a {:href "https://github.com/superkonduktr/LilyHub"}
                   [:img.footer-img-link {:src "/img/GitHub-Mark-32px.png" :alt "LilyHub on Github"}]]]
            [:li [:a {:href "https://twitter.com/lilyhub_co"}
                   [:img.footer-img-link {:src "/img/twitter-32.png" :alt "LilyHub on Twitter"}]]]
            [:li "By " [:a {:href "https://github.com/superkonduktr"} "superkonduktr"] " & "
                      [:a {:href "https://github.com/prepor"} "prepor"] "."]]]]]])
