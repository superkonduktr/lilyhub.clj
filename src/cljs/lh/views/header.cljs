(ns lh.cljs.views.header
  (:require [lh.cljs.session :as session]
            [clojure.string :refer [capitalize]]))

(defn title
  "Sets page title."
  [current]
  (let [current (-> current (name) (capitalize))
        engr-id (-> (session/get :show-engraving) (:id))]
    (if (= current "Engraving")
      [:title (str "LilyHub | #" engr-id)]
      [:title (str "LilyHub | " current)])))

(defn set-active-class
  "A helper that adds .active class to a link if it matches current page,
  and clears its class otherwise."
  [page current]
  (if (or (= (keyword page) current)
          (and (= current :engraving) (= page "browse")))
    "active"
    ""))

(defn header-page-links
  "A helper that creates an unordered list of links for the header."
  [pages current]
  [:ul (->> pages
            (map (fn [page]
                   [:li
                     [:a {:href (str "#/" page)
                          :id (str "header-link-" page)
                          :class (set-active-class page current)}
                      (capitalize page)]])))])

(defn header [current-page]
  [:header.header-navbar
    [title current-page]
    [:div.container
      [:div.row
        [:div.col-lg-offset-1.col-lg-5.col-md-6.col-sm-12.col-xs-12
          [:div.navbar-brand
            [:a {:href "/"} "Lilyhub"]]]
        [:div.col-lg-6.col-md-6.col-sm-12.col-xs-12
          [:nav.header-links
            [header-page-links
             ["create" "browse" "guide" "about"]
             current-page]]]]]])
