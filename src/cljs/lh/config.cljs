(ns lh.cljs.config)

(def init-engraving
  {:text "\\version \"2.18.2\"\n\n\\relative c' { c d e2 }"
   :private false
   :state nil
   :id nil})
