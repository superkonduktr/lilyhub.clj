(ns lh.cljs.session
  (:refer-clojure :exclude [get])
  (:require [reagent.core :as reagent :refer [atom]]))

(def app-state (atom {}))

(defn get [k & [default]]
  (@app-state k default))

(defn get-new-engraving [k & [default]]
  (get-in @app-state [:new-engraving k] default))

(defn put! [k v]
  (swap! app-state assoc k v))

(defn put-new-engraving! [k v]
  (swap! app-state assoc-in [:new-engraving k] v))

;; app-state can contain the following:
;;
;; {:current-page         :create/:browse/:about/etc
;;  :browse-page          any integer > 0
;;  :browse-engravings    '({engr1} {engr2} ... {engr10})
;;  :browse-total         total number of public engravings
;;  :show-engraving
;;  :new-engraving {
;;    :text               string
;;    :private            true/false
;;    :state              "new"/"error"/"completed"/nil
;;  }
;; }
3
