(ns lh.cljs.routes
  (:require [reagent.core :as reagent :refer [atom]]
            [secretary.core :as secretary :refer-macros [defroute]]
            [goog.events :as events]
            [goog.history.EventType :as EventType]
            [lh.cljs.session :as session]
            [lh.cljs.ajax :as ajax])
  (:import goog.History))

(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

(defroute "/" []
  (session/put! :current-page :create))

(defroute "/create" []
  (session/put! :current-page :create))

(defroute "/browse" []
  (session/put! :browse-page 1)
  (secretary/dispatch! "/browse/1"))

(defroute "/browse/:page" [page]
  (let [nums (re-find #"\d+" page)
        page (if (nil? nums) 1 (js/parseInt nums))]
    (session/put! :browse-page page)
    (ajax/browse-page page)))

(defroute "/engravings/:id" [id]
  (ajax/get-engraving id))

(defroute "/guide" []
  (session/put! :current-page :guide))

(defroute "/about" []
  (session/put! :current-page :about))
