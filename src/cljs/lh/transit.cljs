(ns lh.cljs.transit
  (:require [cognitect.transit :as transit]))

(defn to-transit [data]
  (let [w (transit/writer :json)]
    (transit/write w data)))

(defn parse-transit [data]
  (let [r (transit/reader :json)]
    (transit/read r data)))
