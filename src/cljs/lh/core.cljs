(ns lh.cljs.core
  (:require [reagent.core :as reagent :refer [atom]]
            [secretary.core :as secretary]
            [lh.cljs.session :as session]
            [lh.cljs.routes :as routes]
            [lh.cljs.config :refer [init-engraving]]
            [lh.cljs.views.header :refer [header]]
            [lh.cljs.views.footer :refer [footer]]
            [lh.cljs.views.create :refer [create]]
            [lh.cljs.views.engraving :refer [engraving-page]]
            [lh.cljs.views.browse :refer [browse]]
            [lh.cljs.views.guide :refer [guide]]
            [lh.cljs.views.about :refer [about]]))

(defn content
  "Returns page content based on :current-page value in app-state."
  [current-page]
  (let [show-engraving (session/get :show-engraving)
        browse-page (session/get :browse-page)
        browse-engravings (session/get :browse-engravings)]
    [:div.content
      (case current-page
        :create (create)
        :engraving (engraving-page show-engraving)
        :browse (browse browse-page browse-engravings)
        :guide (guide)
        :about (about))]))

(defn debug
  "Scaffold to track :new-engraving's state."
  []
  (let [{text :text
         private :private
         state :state
         id :id} (session/get :new-engraving)]
    [:pre ":new-engraving\n\n"
      [:small
        [:strong ":private "] (str private)
        [:strong "\n:text "] text
        [:strong "\n:state "] state
        [:strong "\n:id "] id]]))

(defn page
  "Main component that brings together all the views."
  []
  (let [current-page (session/get :current-page)]
    [:div.wrapper
      [header current-page]
      ; [debug]
      [content current-page]
      [footer]]))

(defn init-setup
  "Initializes app-state with defaults."
  []
  (let [browse-page (session/get :browse-page)
        new-engraving (session/get :new-engraving)]
    (when (nil? browse-page) (session/put! :browse-page 1))
    (when (nil? new-engraving) (session/put! :new-engraving init-engraving))))

(defn main
  "Launches main component."
  []
  (secretary/set-config! :prefix "#")
  (routes/hook-browser-navigation!)
  (init-setup)
  (reagent/render-component [page] (.getElementById js/document "app")))
