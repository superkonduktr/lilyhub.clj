(ns lh.server
  (:require [clojure.java.io :as io]
            [clojure.tools.logging :as log]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [com.stuartsierra.component :as component]
            [lh.create :refer [create-engraving]]
            [lh.db-query :as db]
            [lh.transit :refer [to-transit]]
            [org.httpkit.server :as http]
            [ring.middleware.transit :refer :all]
            [ring.util.response :as r]
            [lh.helper :refer [humanize-timestamps]]))

(defn transit-response [response]
  (-> response
      (update-in [:body] to-transit)
      (assoc-in [:headers "Content-Type"] "application/transit+json; charset=utf-8")))

(def ^:dynamic *synchrony* false)

(defmacro with-synchrony
  [& body]
  `(binding [*synchrony* true] ~@body))

(defn sync-or-async*
  [req f]
  (if *synchrony*
    (f)
    (http/with-channel req channel
      (future
        (http/send! channel (transit-response (f)))))))

(defmacro sync-or-async
  [req & body]
  `(sync-or-async* ~req (fn [] ~@body)))

(defn app-routes
  [db]
  (routes
   (route/resources "/")

   (GET "/engravings/:id" [id]
     (let [engr (db/get-engraving db id)]
       (if (nil? engr)
         (r/not-found nil)
         (r/response (->> (db/get-errors db id)
                          (assoc engr :error )
                          humanize-timestamps)))))

   (GET "/browse/:page" [page]
     (let [page (Integer. page)
           content (map humanize-timestamps (db/get-page db page))
           total (db/public-engravings-count db)]
       (if (empty? content)
         (r/not-found nil)
         (r/response {:content content :total total}))))

   (GET "/*" [] (io/resource "index.html"))

   (POST "/publish" req
     (let [{:keys [text private]} (:params req)]
       (sync-or-async req
        (r/created "" (create-engraving db text private)))))))

(defrecord HTTP [db server]
  component/Lifecycle
  (start [this]
    (-> (app-routes db)
        (wrap-transit-params)
        (wrap-transit-response)
        (http/run-server {:port 8000})
        (->> (assoc this :server))))
  (stop [this]
    (server)
    this))

(defn new-http [] (map->HTTP {}))
