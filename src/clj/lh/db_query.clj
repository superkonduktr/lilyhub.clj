;; Dedicated namespace for db queries.

(ns lh.db-query
  (:require [clojure.java.jdbc :as clj-jdbc]
            [clojure.tools.logging :as log]
            [honeysql.core :as sql]
            [honeysql.format :as sql-fmt]
            [honeysql.helpers :refer :all]
            [com.stuartsierra.component :as component]))

(defn sql
  [q]
  (sql/format q :quoting :ansi))

(defn query
  "Executes SQL query. Query in provided as a Honey SQL hashmap."
  [q db]
  (clj-jdbc/query db (sql q)))

(defn get-engraving
  "Query for the engraving with given id."
  [db id]
  (-> {:select [:*]
       :from [:engravings]
       :where [:= :id id]
       :limit 1}
      (query db)
      first))

(defn insert-engraving
  "Creates a new engraving with provided text and private/non-private flag.
  Returns new engraving packed in a hashmap."
  [db text private]
  (let [id (str (java.util.UUID/randomUUID))
        ts (->> (java.util.Date.) (.getTime) (java.sql.Timestamp.))
        new-engr {:id id
                  :text text
                  :is_private private
                  :error nil
                  :state "new"
                  :created_at ts
                  :updated_at ts}]
    (clj-jdbc/insert! db :engravings new-engr)
    new-engr))

(defn get-errors
  "Returns a collection of error line/messages for given engraving."
  [db engr-id]
  (-> {:select [:line :msg]
       :from [:errors]
       :where [:= :engraving_id engr-id]
       :order-by [[:id :asc]]}
      (query db)))

(defn insert-errors
  "Inserts error lines and messages for given engraving."
  [db engr-id errors]
  (let [errors (map #(assoc % :engraving_id engr-id) errors)]
    (doseq [e errors]
      (clj-jdbc/insert! db :errors e))))

(defn update-engraving
  "Accepts id of an engraving and a hashmap of values;
  updates respective attributes in db record."
  [db id values]
  (let [ts (->> (java.util.Date.) (.getTime) (java.sql.Timestamp.))
        values (assoc values :updated_at ts)]
    (clj-jdbc/update! db :engravings values ["id = ?" id])))

(defn public-engravings-count
  "Returns total number of completed public engravings in :engravings table."
  [db]
  (-> {:select [:%count.id]
       :from [:engravings]
       :where [:and [:= :is_private false] [:= :state "completed"]]}
       (query db)
       first
       :count))

(defn get-page
  "Query for 10 completed non-private engravings on given page."
  ([db] (get-page db 1))
  ([db p]
     (-> {:select [:*]
          :from [:engravings]
          :where [:and [:= :is_private false] [:= :state "completed"]]
          :order-by [[:created_at :desc]]
          :limit 10
          :offset (* 10 (- p 1))}
         (query db))))
