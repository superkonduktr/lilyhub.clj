(ns lh.helper
  (require [clojure.contrib.humanize :as humanize]))

(defn humanize-timestamps
  "Accepts an engraving hashmap,
  transforms its :created_at and :updated_at timestamps
  into human friendly 'time ago' format."
  [engr]
  (when-not (empty? engr)
    (let [created (-> engr (:created_at) (humanize/datetime))
          updated (-> engr (:updated_at) (humanize/datetime))]
      (assoc engr :created_at created :updated_at updated))))