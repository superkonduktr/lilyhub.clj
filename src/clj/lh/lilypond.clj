(ns lh.lilypond
  (:require [clojure.java.shell :refer [sh]]
            [clojure.tools.logging :as log]))

(defn parse-exit
  [e]
  (if (= 0 e)
    "completed"
    "error"))

(defn parse-errors
  "Extracts error messages from LilyPond stderr output."
  [e]
  (->> e
       (clojure.string/split-lines)
       (filter #(re-find #": error:" %))
       (map (fn [s]
              (let [[_ line msg] (re-matches #"^.?:(\d+):\d+:\serror:\s(.*)" s)]
                {:line (Integer. line) :msg msg})))))

(defn render
  "Calls LilyPond to render given code,
  returns a neat hashmap with :status and :error."
  [text id]
  (let [exec-path "lilypond"
        out-path "resources/public/img/engravings/"
        cmd (sh exec-path
                "--png"
                "-dpreview"
                "-o" (str out-path id)
                "-"
                :in text)]
    (log/info "Render done.")
    {:status (parse-exit (:exit cmd))
     :error (parse-errors (:err cmd))}))
