(ns lh.system
  (:require [com.stuartsierra.component :as component]
            [lh.server :as server]
            [lh.db-config :refer [new-jdbc dev-spec test-spec]]))

(defn system
  ([] (system :dev))
  ([env]
    (let [spec (case env :test test-spec dev-spec)]
      (-> (component/system-map
            :db (new-jdbc spec)
            :http (server/new-http))
          (component/system-using {:http [:db]})))))