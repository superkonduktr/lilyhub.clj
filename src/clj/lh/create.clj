(ns lh.create
  (:require [clojure.tools.logging :as log]
            [lh.db-query :as db]
            [lh.lilypond :refer [render]]
            [lh.transit :refer [to-transit]])
  (:import [java.io File]))

(defn update-state
  "Updates state of engraving with given id."
  [db id state]
  (db/update-engraving db id {:state state}))

(defn cleanup
  "Removes unnecessary engraving files."
  [id]
  (let [out-path "resources/public/img/engravings/"]
    (.delete (File. (str out-path id ".preview.eps")))
    (.delete (File. (str out-path id ".png")))
    (.renameTo (File. (str out-path id ".preview.png"))
               (File. (str out-path id ".png")))))

(defn create-engraving
  "Accepts new engraving's text and private/non-private flag,
  then inserts a new record into db and invokes LilyPond render.
  Updates :state to 'completed' upon successful render,
  and to 'error' on parsing error.
  In the latter case inserts respective rows into :errors table.
  Returns engraving hashmap in its final appearance."
  [db text private]
  (let [engr (db/insert-engraving db text private)
        id (:id engr)
        render (render text id)
        state (:status render)
        error (:error render)]
    (log/info "Rendered engraving #" id)
    (when-not (empty? error)
      (db/insert-errors db id error))
    (update-state db id state)
    (cleanup id)
    (assoc engr :state state :error error)))










