(ns lh.transit
  (:require [cognitect.transit :as transit])
  (:import [java.io ByteArrayInputStream ByteArrayOutputStream]))

;; Helpers for transit exchange.

(defn parse-transit [data]
  (let [b   (.getBytes data)
        in  (ByteArrayInputStream. b)
        r   (transit/reader in :json)
        ret (transit/read r)]
    ret))

(defn to-transit [data]
  (let [out (ByteArrayOutputStream.)
        w   (transit/writer out :json)
        _   (transit/write w data)
        ret (.toString out)]
    (.reset out)
    ret))
