  (defproject lilyhub "0.1.0-SNAPSHOT"
  :description "CljilyCljub"
  :url "http://lilyhub.co/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :source-paths ["src/clj" "dev"]

  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojurescript "0.0-2371"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [org.clojure/tools.logging "0.3.1"]
                 [leiningen "2.5.0"]
                 [ring "1.3.1"]
                 [ring/ring-defaults "0.1.2"]
                 [ring-transit "0.1.3"]
                 [compojure "1.3.1"]
                 [http-kit "2.1.16"]
                 [org.clojure/java.jdbc "0.3.6"]
                 [postgresql/postgresql "8.4-702.jdbc4"]
                 [com.mchange/c3p0 "0.9.2.1"]
                 [honeysql "0.4.3"]
                 [secretary "1.2.1"]
                 [garden "1.2.5"]
                 [com.stuartsierra/component "0.2.2"]
                 [figwheel "0.1.4-SNAPSHOT"]
                 [environ "1.0.0"]
                 [reagent "0.4.2"]
                 [cljs-ajax "0.3.3"]
                 [com.cognitect/transit-clj "0.8.259"]
                 [com.cognitect/transit-cljs "0.8.194"]
                 [clojure-humanize "0.1.0"]]

  :min-lein-version "2.5.0"

  :plugins [[lein-cljsbuild "1.0.3"]
            [lein-environ "1.0.0"]
            [lein-figwheel "0.1.4-SNAPSHOT"]
            [lein-garden "0.2.5"]
            [clj-sql-up "0.3.4"]]

  :figwheel {:http-server-root "public"
             :port 3449}

  :cljsbuild {:builds {:app {:source-paths ["src/cljs" "dev"]
                             :compiler {:output-to "resources/public/js/app.js"
                                        :output-dir "resources/public/js/out"
                                        :source-map    "resources/public/js/out.js.map"
                                        :optimizations :none}}}}
  :garden {:builds [{:id "style"
                     :source-paths ["src/css"]
                     :stylesheet lh.css.main/style
                     :compiler {:output-to "resources/public/css/style.css"
                                :pretty-print? true}}]}

  :clj-sql-up {:database-dev {:subprotocol "postgresql"
                               :subname "//localhost:5432/spruce_development"
                               :classname "org.postgresql.Driver"
                               :user "spruce"
                               :password "spruce"}
               :database-test {:subprotocol "postgresql"
                               :subname "//localhost:5432/spruce_test"
                               :classname "org.postgresql.Driver"
                               :user "spruce"
                               :password "spruce"}
               :deps [[postgresql/postgresql "8.4-702.jdbc4"]]}

  :profiles {:dev
             {:dependencies [[ring/ring-mock "0.2.0"]]}})